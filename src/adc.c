#include <stm32f4xx_gpio.h>
#include <stm32f4xx_tim.h>
#include <stm32f4xx_rcc.h>
#include "stm32f4xx_adc.h"
#include "stm32f4xx_dma.h"

__IO uint32_t uhADC1ConvertedValue;

unsigned int getADCVal(){
    return uhADC1ConvertedValue;
}


void adc_init(void)
{
    GPIO_InitTypeDef      GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
    RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

    //konfiguracja ADC
    ADC1->CR2 = ADC_CR2_ADON |                          //włącz ADC
                ADC_CR2_EXTEN_0 |                       //wyzwalanie przetwornika zboczem opadającym i narastającym
                ADC_CR2_EXTSEL_3 | ADC_CR2_EXTSEL_0 |   //wyzwalanie przetwornika kanałem 4 timera 4
                ADC_CR2_DDS |                           //kontynuuj przesył DMA po ostatnim przesyle (konieczne dla circular mode)
                ADC_CR2_DMA;                            //włącz DMA dla ADC

    //włączenie skanowania i przerwania dla zakonczonej konwersji
    ADC1->CR1 = ADC_CR1_SCAN; //| ADC_CR1_EOCIE;

    //Ustawienie czasu konwersji na 3 + 12 cykli zegara ADC, zegar ADC == 42MHz częstotliwosć próbkowania ~1Ms
    ADC1->SMPR1 = 0;//ADC_SMPR1_SMP11_1 | ADC_SMPR1_SMP12_1;

    //Ustawienie ilosci kanałów do skanowania
    ADC1->SQR1 = (1)<<20;

    //Ustawienie kanałów 11 i 12 do skanowania
    ADC1->SQR3 = 10<<5;

    //Konfiguracja DMA dla przetwornika ADC1
    DMA2_Stream0->NDTR = 1;                 //ilosc bajtów do przesłania
    DMA2_Stream0->PAR = (uint32_t)&ADC1->DR;
    DMA2_Stream0->M0AR = (uint32_t)&uhADC1ConvertedValue;

    DMA2_Stream0->CR =  DMA_SxCR_PL_1 |     //priority high
                        DMA_SxCR_MSIZE_0 |  //memory size 16bit
                        DMA_SxCR_PSIZE_0 |  //pheriperial size 16bit
                        DMA_SxCR_MINC |     //inkrementuj wskaźnik po stronie pamięci
                        DMA_SxCR_CIRC |     //zapętlenie bufora pamięci
                        DMA_SxCR_EN;        //włączenie kontrolera DMA

    //konfiguracja timera dla przetwornika
    RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
    TIM4->CR1 = 0;                          //resetowanie rejestru konfiguracji
    TIM4->PSC = 0;                          //prescaller na 0
    TIM4->ARR = 167;                        //reload register na 83
    TIM4->CCR4 = 167;                       //rejesrt compare dla wyzwalania przetwornika
    TIM4->CCMR2 |= TIM_CCMR2_OC4M_0 | TIM_CCMR2_OC4M_1; //przełączanie wyjcia przy compare
    TIM4->CCER |= TIM_CCER_CC4E;            //włączenie wyjcia 4

    //konfiguracja NVIC
    NVIC_EnableIRQ(ADC_IRQn);               //przerwanie od zakonczenia konwersji ADC

    //uruchom przetwornik
    ADC1->CR2 |= ADC_CR2_ADON;

    //uruchom timer
    TIM4->CR1 = TIM_CR1_CEN;


}
