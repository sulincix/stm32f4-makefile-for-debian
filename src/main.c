#include <stm32f4xx_gpio.h>
#include <stm32f4xx_tim.h>
#include <stm32f4xx_rcc.h>
#include "stm32f4xx_adc.h"
#include "stm32f4xx_dma.h"
#include <misc.h>
#include "tim.c"
#include "adc.c"
#include "main.h"


#include "stm32f4_discovery.h"

uint16_t Period;

void delay(uint32_t time){
		while(time--);
}

void RCC_Configuration(void){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
}

/**************************************************************************************/
int main(void)
{
	RCC_Configuration();
	TIM_INIT();
	adc_init();
	
	int val=0;
	while(1){
		    int var=getADCVal();
			TIM1->CCR1 = (var*Period)/4096;
			val++;
			if(val>3){
					val=0;
			}
			delay(168000);
	}
}
/**************************************************************************************/
#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line){
	while (1);
}
#endif
