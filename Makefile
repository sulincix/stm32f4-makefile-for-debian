PROJ_NAME=blinky
# http://www.st.com/web/en/catalog/tools/PF257904
STM_DIR=/opt/STM32F4-Discovery_FW_V1.1.0
STM_SRC = $(STM_DIR)/Libraries/STM32F4xx_StdPeriph_Driver/src
vpath %.c $(STM_SRC)
SRCS   = src/main.c
SRCS  += src/system_stm32f4xx.c
SRCS  += stm32f4xx_rcc.c
SRCS  += stm32f4xx_dma.c
SRCS  += stm32f4xx_adc.c
SRCS  += stm32f4xx_gpio.c
SRCS  += stm32f4xx_tim.c
SRCS += $(STM_DIR)/Libraries/CMSIS/ST/STM32F4xx/Source/Templates/TrueSTUDIO/startup_stm32f4xx.s
INC_DIRS  = $(STM_DIR)/Utilities/STM32F4-Discovery
INC_DIRS += $(STM_DIR)/Libraries/CMSIS/Include
INC_DIRS += $(STM_DIR)/Libraries/CMSIS/ST/STM32F4xx/Include
INC_DIRS += $(STM_DIR)/Libraries/STM32F4xx_StdPeriph_Driver/inc
INC_DIRS += src
INC_DIRS += .
TOOLS_DIR = /usr/bin
CC      = $(TOOLS_DIR)/arm-none-eabi-gcc
OBJCOPY = $(TOOLS_DIR)/arm-none-eabi-objcopy
GDB     = $(TOOLS_DIR)/arm-none-eabi-gdb
INCLUDE = $(addprefix -I,$(INC_DIRS))
DEFS    = -DUSE_STDPERIPH_DRIVER
CFLAGS  = -ggdb
CFLAGS += -O0 
CFLAGS += -Wall -Wextra -Warray-bounds
CFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
LFLAGS  = -T src/stm32_flash.ld


.PHONY: $(PROJ_NAME)
$(PROJ_NAME): $(PROJ_NAME).elf

$(PROJ_NAME).elf: $(SRCS)
	mkdir build || true
	$(CC) $(INCLUDE) $(DEFS) $(CFLAGS) $(LFLAGS) $^ -o build/$@ 
	$(OBJCOPY) -O ihex build/$(PROJ_NAME).elf   build/$(PROJ_NAME).hex
	$(OBJCOPY) -O binary build/$(PROJ_NAME).elf build/$(PROJ_NAME).bin

clean:
	rm -rf *.o build

flash: 
	st-flash write build/$(PROJ_NAME).bin 0x8000000

.PHONY: debug
debug:
	$(GDB) $(PROJ_NAME).elf
